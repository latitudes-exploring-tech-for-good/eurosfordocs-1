from pycodestyle import StyleGuide, normalize_paths

COMMA_SEPARATED_EXCLUDE_PATHS = "venv,bundle,notebooks"


def test_coding_style():
    """Test that we conform to PEP-8."""
    style = StyleGuide(config_file='setup.cfg', quiet=False)
    style.options.exclude = normalize_paths(COMMA_SEPARATED_EXCLUDE_PATHS)
    result = style.check_files('.')
    assert result.total_errors == 0, \
        "Found {} code style errors (and warnings).\n" \
        "Run 'pycodestyle --exclude={} .' to see the details.".format(result.total_errors,
                                                                      COMMA_SEPARATED_EXCLUDE_PATHS)
