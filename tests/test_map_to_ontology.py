import pandas as pd
from anytree import AnyNode

from src.constants import ontology
from src.specific.map_to_ontology import get_reversed_dict, get_strict_mapper_to_category, \
    get_similarity_mapper_to_category, get_mapper_to_fist_level_parent


def test_get_reversed_dict():
    # Given
    value_to_list = {
        'a': [1, 2, 3],
        2: ['b', 'c']
    }
    expected_reversed_dict = {
        1: 'a',
        2: 'a',
        3: 'a',
        'b': 2,
        'c': 2,
    }

    # When
    actual_reversed_dict = get_reversed_dict(value_to_list)

    # Then
    assert actual_reversed_dict == expected_reversed_dict


def test_get_strict_mapper_to_category():
    mapping = get_strict_mapper_to_category()

    assert mapping['HOSPITALITE'] == ontology.HOSPITALITY
    assert mapping['dqsfadfqsdfadfa'] == ontology.UNSUCESSFULL_MAPPING


def test_get_similarity_mapper_to_category():
    misspelled_gift = 'CADEA'
    s_detail = pd.Series([misspelled_gift])
    mapping = get_similarity_mapper_to_category(s_detail)

    assert mapping['HOSPITALITE'] == ontology.HOSPITALITY
    assert mapping['DQDADOIVWX'] == ontology.UNSUCESSFULL_MAPPING
    assert mapping[misspelled_gift] == ontology.GIFT


def test_get_mapper_to_fist_level_parent():
    # Given
    root = AnyNode(id="root")
    s0 = AnyNode(id="sub0", parent=root)
    s0b = AnyNode(id="sub0B", parent=s0)
    s0a = AnyNode(id="sub0A", parent=s0)
    s1 = AnyNode(id="sub1", parent=root)
    s1a = AnyNode(id="sub1A", parent=s1)
    s1b = AnyNode(id="sub1B", parent=s1)
    s1c = AnyNode(id="sub1C", parent=s1)
    s1ca = AnyNode(id="sub1Ca", parent=s1c)

    fist_level_nodes = {s0, s1}
    mapping_size = 8

    # When
    mapping = get_mapper_to_fist_level_parent(root)

    # Then
    assert set(mapping.values()) == fist_level_nodes
    assert len(mapping) == mapping_size
    assert mapping[s0] == s0
    assert mapping[s0b] == s0
    assert mapping[s1ca] == s1
