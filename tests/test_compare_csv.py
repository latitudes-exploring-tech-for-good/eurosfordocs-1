import pandas as pd
import pytest

from src.utils import compare_csv as cc


def test_assert_columns_are_equals():
    with pytest.raises(AssertionError):
        cc.assert_csv_have_same_columns(*cc.get_fixture_csv_pathes(cc.DIFFERENT_COLUMNS_SETS))

    with pytest.raises(AssertionError):
        cc.assert_csv_have_same_columns(*cc.get_fixture_csv_pathes(cc.DIFFERENT_COLUMNS_ORDER))


def test_assert_csv_have_same_content():
    with pytest.raises(AssertionError):
        cc.assert_csv_have_same_content(*cc.get_fixture_csv_pathes(cc.DIFFERENT_LINE_NUMBER))

    with pytest.raises(AssertionError):
        cc.assert_csv_have_same_content(*cc.get_fixture_csv_pathes(cc.DIFFERENT_CONTENT))


def test_get_difference_position():
    # Given
    csv_path_1, csv_path_2 = cc.get_fixture_csv_pathes(cc.DIFFERENT_CONTENT)
    df1 = pd.read_csv(csv_path_1, sep=';')
    df2 = pd.read_csv(csv_path_2, sep=';')

    expected_difference = pd.DataFrame(columns=["column2", "column3"],
                                       index=[1, 2],
                                       data=[[cc.DIFFERENCE, ""],
                                             ["", cc.DIFFERENCE]]
                                       )
    # When
    actual_difference = cc.get_difference_position(df1, df2)

    # Then
    assert expected_difference.equals(actual_difference)
