from os.path import join as p_join

import pandas as pd

from src.constants import csv
from src.constants.directories import TEST, DATA, ACTUAL
from src.specific.declaration.join_convention_to_other_declarations import build_aggregated_linked_declarations, \
    add_linked_declarations, compute_amounts
from src.utils.utils import add_final_directory_to_file_path

TEST_DATA_DIR = p_join(TEST, DATA, 'join_convention_to_other')

DETAILED_LINKED_REMUNERATIONS_CSV_PATH = p_join(TEST_DATA_DIR, 'detailed_linked_remunerations.csv')
AGGREGATED_LINKED_REMUNERATIONS_CSV_PATH = p_join(TEST_DATA_DIR, 'aggregated_linked_remunerations.csv')
CONVENTION_CSV_PATH = p_join(TEST_DATA_DIR, 'convention.csv')
CONVENTION_WITH_REMUNERATION_CSV_PATH = p_join(TEST_DATA_DIR, 'convention_with_remunerations.csv')
CONVENTION_WITH_DECLARATIONS_CSV_PATH = p_join(TEST_DATA_DIR, 'convention_with_declarations.csv')
CONVENTION_WITH_COMPUTED_AMOUNTS_CSV_PATH = p_join(TEST_DATA_DIR, 'convention_with_computed_amounts.csv')


def write_actual_df(actual_df, expected_csv_path):
    actual_csv_path = add_final_directory_to_file_path(expected_csv_path, ACTUAL)
    actual_df.to_csv(actual_csv_path, index=False, sep=csv.DEFAULT_SEPARATOR)


def test_build_aggregated_linked_declarations():
    # Given
    expected_csv_path = AGGREGATED_LINKED_REMUNERATIONS_CSV_PATH
    expected_df = pd.read_csv(expected_csv_path,
                              sep=csv.DEFAULT_SEPARATOR, index_col=0)

    # When
    actual_df = build_aggregated_linked_declarations(DETAILED_LINKED_REMUNERATIONS_CSV_PATH)
    write_actual_df(actual_df, expected_csv_path)

    # Then
    assert expected_df.equals(actual_df)


def test_add_linked_declarations():
    # Given
    df_convention = pd.read_csv(CONVENTION_CSV_PATH, sep=csv.DEFAULT_SEPARATOR)
    df_aggregated_linked_remunerations = pd.read_csv(AGGREGATED_LINKED_REMUNERATIONS_CSV_PATH,
                                                     sep=csv.DEFAULT_SEPARATOR, index_col=0)
    expected_csv_path = CONVENTION_WITH_REMUNERATION_CSV_PATH
    expected_df = pd.read_csv(expected_csv_path, sep=csv.DEFAULT_SEPARATOR)

    # When
    actual_df = add_linked_declarations(df_convention, df_aggregated_linked_remunerations)
    write_actual_df(actual_df, expected_csv_path)

    # Then
    assert expected_df.equals(actual_df)


def test_compute_amounts():
    # Given
    df_convention_with_declarations = pd.read_csv(CONVENTION_WITH_DECLARATIONS_CSV_PATH, sep=csv.DEFAULT_SEPARATOR)
    expected_csv_path = CONVENTION_WITH_COMPUTED_AMOUNTS_CSV_PATH
    expected_df = pd.read_csv(CONVENTION_WITH_COMPUTED_AMOUNTS_CSV_PATH, sep=csv.DEFAULT_SEPARATOR)

    # When
    actual_df = compute_amounts(df_convention_with_declarations)
    write_actual_df(actual_df, expected_csv_path)

    # Then
    assert expected_df.equals(actual_df)
