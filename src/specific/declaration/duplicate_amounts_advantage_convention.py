from src.constants import column, categorical, tables
from src.generic.ddl.materialized_view import MaterializedViewDDL
from src.utils.utils import merge_dict

doublons_select_query = """
    WITH a AS (
        SELECT
             {COMPANY_ID},
             COALESCE({NAME_SURNAME}, 'Ø') as {NAME_SURNAME},
             COALESCE({BENEFICIARY_ORGANIZATION_NAME}, 'Ø') as {BENEFICIARY_ORGANIZATION_NAME},
             CAST({DATE} AS date),
             {DECLARATION_TYPE},
             sum({AMOUNT}) as {AMOUNT}
        FROM {DECLARATION}
        WHERE
            {DECLARATION_TYPE} != '{REMUNERATION}'
        GROUP BY
             {COMPANY_ID},
             {NAME_SURNAME},
             {BENEFICIARY_ORGANIZATION_NAME},
             CAST({DATE} AS date),
             {DECLARATION_TYPE}
    ),
    advantage AS (SELECT * FROM a WHERE a.{DECLARATION_TYPE} = '{ADVANTAGE}'),
    convention AS (SELECT * FROM a WHERE a.{DECLARATION_TYPE} = '{CONVENTION}'),
    doublons AS (
        SELECT *
        FROM advantage INNER JOIN convention
          USING ({NAME_SURNAME}, {BENEFICIARY_ORGANIZATION_NAME}, {DATE}, {COMPANY_ID}, {AMOUNT})
    )
    SELECT
       company.filiale_déclarante,
       company.entreprise_émmetrice,
       doublons.{COMPANY_ID},
       doublons.{NAME_SURNAME},
       doublons.{BENEFICIARY_ORGANIZATION_NAME},
       doublons.{DATE},
       doublons.{AMOUNT}
    FROM doublons LEFT JOIN {COMPANY_DIRECTORY} as company using ({COMPANY_ID})
"""

constant_dict = merge_dict([column.__dict__, categorical.__dict__, tables.__dict__])

materialized_view = MaterializedViewDDL(
    name=tables.DUPLICATES_AMOUNT_ADVANTAGE_CONVENTION,
    select_query=doublons_select_query.format(**constant_dict),
    search_indices=[column.COMPANY, column.NAME_SURNAME, column.BENEFICIARY_ORGANIZATION_NAME]
)

if __name__ == '__main__':
    materialized_view.reset()
    materialized_view.run()
