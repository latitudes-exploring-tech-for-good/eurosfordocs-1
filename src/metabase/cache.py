import logging

from sqlalchemy import create_engine, text

from settings import MB_POSTGRES_HOST, MB_POSTGRES_PORT, MB_POSTGRES_PASSWORD

MB_POSTGRES_USER = "postgres"

MB_POSTGRES_URL = "postgresql://{user}:{password}@{host}:{port}".format(user=MB_POSTGRES_USER,
                                                                        password=MB_POSTGRES_PASSWORD,
                                                                        host=MB_POSTGRES_HOST,
                                                                        port=MB_POSTGRES_PORT)


def delete_metabase_cache():
    logging.info("Delete Metabase cache (truncate query_cache table)")
    engine = create_engine(MB_POSTGRES_URL)
    query = text("TRUNCATE TABLE public.query_cache")
    engine.execute(query)


if __name__ == '__main__':
    delete_metabase_cache()
