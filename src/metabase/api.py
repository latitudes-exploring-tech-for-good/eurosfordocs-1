import logging
import sys
from time import sleep
from typing import List, Union

import requests

from settings import METABASE_USERNAME, METABASE_PASSWORD, METABASE_BASE_URL


def metabase_get_token() -> str:
    payload = {"username": METABASE_USERNAME, "password": METABASE_PASSWORD}
    r = requests.post(METABASE_BASE_URL + "/api/session", json=payload)
    if r.status_code != 200:
        raise ValueError("We could not get token. Endpoint returned with status code '{}', with message : '{}'"
                         .format(r.status_code, r.text[:300]))
    return r.json()['id']


def metabase_get(endpoint: str, token: str) -> Union[dict, List[dict]]:
    r = requests.get(METABASE_BASE_URL + endpoint, headers={"X-Metabase-Session": token})
    if r.status_code != 200:
        raise ValueError("Get requests on endpoint '{}' returned with non 200 status code '{}' and message '{}'"
                         .format(endpoint, r.status_code, r.text[:300]))
    return r.json()


def metabase_post(endpoint: str, token: str, payload: dict = None) -> dict:
    if payload is None:
        payload = {}
    r = requests.post(METABASE_BASE_URL + endpoint, headers={"X-Metabase-Session": token}, json=payload)
    if r.status_code != 200:
        raise ValueError("Post requests on endpoint '{}' returned with non 200 status code '{}' and message '{}'"
                         .format(endpoint, r.status_code, r.text[:300]))
    return r.json()


def metabase_query_card(card_id: int, token: str, ignore_cache: bool = True) -> dict:
    endpoint = "/api/card/{}/query".format(card_id)
    payload = {"ignore_cache": ignore_cache}
    return metabase_post(endpoint, token, payload)


def metabase_query_cards(cards: List[dict], token: str, ignore_cache: bool = True) -> None:
    for card in sorted(cards, key=lambda x: x['id']):
        result = metabase_query_card(card['id'], token, ignore_cache)
        if result['status'] == 'completed':
            logging.info("Question completed in {time:>4}ms - {id:>3}: {name} "
                         .format(id=card['id'], name=card['name'], time=result['running_time']))
        else:
            logging.warning("Question failed - {id:>3}: {name} - {url}/question/{id}"
                            .format(id=card['id'], name=card['name'], url=METABASE_BASE_URL))


def find_dashboards_containing_card(card_id: int, token: str) -> List[int]:
    dashboards_containing_card = list()
    shallow_dashboard_list = metabase_get("/api/dashboard/", token)
    for shallow_dashboard in shallow_dashboard_list:
        dashboard_id = shallow_dashboard['id']
        dashboard = metabase_get("/api/dashboard/{}".format(dashboard_id), token)
        for ordered_card in dashboard['ordered_cards']:
            if ordered_card['card_id'] == card_id:
                logging.info("Card {card_id} is present in dashboard {id:>3} : {name}"
                             .format(card_id=card_id, id=dashboard_id, name=dashboard['name']))
                dashboards_containing_card.append(dashboard_id)

    if not dashboards_containing_card:
        logging.info("Card {} was not found in any active dashboard".format(card_id))
    return dashboards_containing_card


def get_dashboards_in_root_collection(token) -> List[int]:
    dashboard_list = metabase_get("/api/dashboard/", token)
    for dashboard in dashboard_list:
        if dashboard['collection_id'] is None:
            yield dashboard['id']


def get_cards_in_dashboard_list(dashboard_id_list: List[int], token) -> List[dict]:
    cards = dict()
    for dashboard_id in dashboard_id_list:
        dashboard = metabase_get("/api/dashboard/{}".format(dashboard_id), token)
        for card_container in dashboard['ordered_cards']:
            card = card_container['card']
            if 'id' in card:
                cards[card['id']] = card

    return list(cards.values())


def connect_metabase_query_dashboards_cards_in_root_collection():
    token = metabase_get_token()
    dashboards_in_root_collection = get_dashboards_in_root_collection(token)
    cards = get_cards_in_dashboard_list(dashboards_in_root_collection, token)
    metabase_query_cards(cards, token, ignore_cache=False)


def connect_metabase_query_all_cards():
    token = metabase_get_token()
    cards = metabase_get("/api/card/", token)
    metabase_query_cards(cards, token)


def wait_for_metabase(max_time=300):
    logging.info("Waiting until Metabase accept connexions")
    for i in range(max_time):
        try:
            r = requests.get(METABASE_BASE_URL + "/api/health")
            if r.status_code == 200:
                sys.stdout.write('\n')
                sys.stdout.flush()
                return True
        except (requests.exceptions.ConnectionError, ValueError):
            pass
        sys.stdout.write('.')
        sys.stdout.flush()
        sleep(1)

    logging.info("Exceeding max waiting time of {}s".format(max_time))
    return False


if __name__ == '__main__':
    wait_for_metabase()
    connect_metabase_query_dashboards_cards_in_root_collection()
    find_dashboards_containing_card(7, metabase_get_token())
