DECLARATION_REMUNERATION = 'declaration_remuneration'
DECLARATION_ADVANTAGE = 'declaration_avantage'
DECLARATION_CONVENTION = 'declaration_convention'
COMPANY_DIRECTORY = 'entreprise'
HEALTH_PROFESSIONAL_DIRECTORY = 'annuaire'
HEALTH_ESTABLISHMENT_DIRECTORY = 'etablissement_sante'
HEALTH_PROFESSIONAL_ACTIVITY = 'activite_professionnel_sante'

# View
DECLARATION = 'declaration'

# Materialized view
DUPLICATES_AMOUNT_ADVANTAGE_CONVENTION = "doublons_montants_avantage_convention"
