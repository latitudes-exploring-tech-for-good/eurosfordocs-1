#!/usr/bin/env bash
# Backup metabase data from host to local file
set -euo pipefail

tag=${1:-""}
host=${2:-"ts-prod"}

BACKUP_FILE=backup/pg_dump_${host}_`date +%Y-%m-%d_%H-%M-%S`_${tag}.sql
echo Dump metabase backend on ${host} to ${BACKUP_FILE}
echo You will be prompted to give the password for Postgres metabase backend

pg_dump -h ${host} -p 6543 -U postgres > ${BACKUP_FILE}

echo Done.