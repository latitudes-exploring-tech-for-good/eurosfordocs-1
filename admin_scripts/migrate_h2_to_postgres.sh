#!/usr/bin/env bash
# This script only needs to be run once, to migrate from h2 to postgres as Metabase backend

# Stop metabase
docker-compose stop metabase
docker-compose rm metabase

# (Re)start fresh mb-postgres
docker-compose stop mb-postgres
docker-compose rm mb-postgres

echo You are going to delete all data in mb-posgres
read -p "Continue (I confirm/ No)? " choice
case "$choice" in
  "I confirm") echo "Yes. Going on" ;;
  n|N|no|No ) echo "No. Aborting" ; exit ;;
  * ) echo "invalid. Aborting" ; exit ;;
esac

rm -rf docker/mb-postgres/data

docker-compose up -d mb-postgres

# Run migration from metabase h2 backend to mb-postgres backend
docker-compose run --name metabase-migration \
    -v `pwd`"/docker/metabase/data:/metabase-data" \
    -e "MB_DB_FILE=/metabase-data/metabase.db" \
    metabase load-from-h2

# Start metabase on mb-postgres
docker-compose up -d metabase