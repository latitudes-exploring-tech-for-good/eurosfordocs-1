#!/usr/bin/env bash

host=${1:-"ts-prod"}

ssh -t root@${host} "cd transparence-sante && git pull && docker-compose up website"
