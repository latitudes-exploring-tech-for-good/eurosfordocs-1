
CREATE TABLE "etablissement_sante" (
    "id" SERIAL PRIMARY KEY,
    "adresse" TEXT,
    "cedex" TEXT,
    "code_commune" TEXT,
    "code_postal" TEXT,
    "commune" TEXT,
    "complement_destinataire" TEXT,
    "complement_point_geographique" TEXT,
    "email" TEXT,
    "enseigne_commerciale_site" TEXT,
    "finess_etablissement_juridique" TEXT,
    "finess_site" TEXT,
    "identifiant_organisation" TEXT,
    "indice_repetition_voie" TEXT,
    "mention_distribution" TEXT,
    "numero_voie" TEXT,
    "pays" TEXT,
    "raison_sociale_site" TEXT,
    "siren_site" TEXT,
    "siret_site" TEXT,
    "telecopie" TEXT,
    "telephone" TEXT,
    "telephone_2" TEXT,
    "type_voie" TEXT,
    "voie" TEXT
);
