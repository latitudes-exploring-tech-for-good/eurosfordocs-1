---
permalink: /
title: Bienvenue
layout: splash
header:
  overlay_color: "#000"
  overlay_filter: "0.2"
  overlay_image: assets/images/trust-transparency.jpg?maxwidth=573
  caption: "[credit](http://www.eu-patient.eu/News/News/transparency-is-our-legitimacy-currency-lets-protect-it/)"
excerpt: "Pour la transparence du lobbying des industries de santé."

feature_row_1:
  - image_path: /assets/images/pharma_papers.jpg
    alt: "PHARMA PAPERS. Lobbying et mégaprofits. Tout ce que les laboratoires pharmaceutiques voudraient vous cacher"
    excerpt: "

**13 novembre 2018**
    
    
C'est notamment grâce à EurosForDocs que les journaux en ligne Basta! et l'Observatoire des multinationales publient les [« Pharma Papers »](https://www.bastamag.net/webdocs/pharmapapers/), une série d'enquêtes sur le lobbying et les mégaprofits des laboratoires pharmaceutiques.
    
    
EurosForDocs a été [rendu publique](https://www.bastamag.net/webdocs/pharmapapers/l-argent-de-l-influence/eurosfordocs-une-base-de-donnees-d-utilite-publique/) 
    lors de la parution du premier volet : [« L’argent de l’influence »](https://www.bastamag.net/webdocs/pharmapapers/l-argent-de-l-influence/).
    
    
Cette enquête a fait la [Une](https://www.franceculture.fr/emissions/journal-de-8-h/journal-de-8h-du-mardi-13-novembre-2018) 
    de la matinale de France Culture. 
    "
    
feature_row_2:
  - image_path: /assets/images/implant_files_le_monde.jpg
    alt: "ImplantFiles - Photo d'un TAVI"
    excerpt: "

**28 novembre 2018**


Les journalistes du Monde ont utilisé EurosForDocs durant l'enquête 
[« Implants médicaux : les industriels sont désormais dans le bloc opératoire »](https://www.lemonde.fr/implant-files/article/2018/11/28/implants-medicaux-les-industriels-sont-desormais-dans-le-bloc-operatoire_5389563_5385406.html).
    
    
Dans la partie « Des essais cliniques très intéressés », ils montrent comment les experts chargés d'évaluer l'intérêt du TAVI ont été personnellement financés par Medtronic.
"

feature_row_3:
    - image_path: /assets/images/mediapart_mediacites.png
      alt: "Logo Mediapart et Médiacités"
      excerpt: "

**13 mars 2019** : Des victimes d'un médicament anti-calvitie attaquent le laboratoire MSD.


**3 avril 2019** : Des victimes d'un médicament anti-pilosité attaquent le laboratoire Bayer.


Dans des articles publiés sur Mediapart 
([1](https://www.mediapart.fr/journal/france/130319/les-victimes-d-un-traitement-anti-calvitie-poursuivent-le-laboratoire-msd),
[2](https://www.mediapart.fr/journal/france/030419/les-victimes-d-un-medicament-antipilosite-poursuivent-le-laboratoire-bayer)) 
et [Médiacités](https://www.mediacites.fr/lyon/enquete-lyon/2019/04/03/scandale-androcur-les-victimes-du-medicament-anti-pilosite-attaquent-bayer/), 
la journaliste Rozenn Le Saint déchiffre les conflits d'intérêts ayant mené aux mésusages de ces médicaments.


EurosForDocs permet notamment de mesurer l'ampleur des liens d'intérêts entre 
la Société française de dermatologie et le laboratoire MSD d'une part,
et Société française d’endocrinologie et le laboratoire Bayer d'autre part.

"

feature_row_4:
    - image_path: /assets/images/prescrire.jpg
      alt: "Logo de Prescrire"
      excerpt: "

**avril 2019**


La revue médicale indépendante Prescrire rappelle 
l'[utilité des bases de données de type Transparence Santé](/assets/download/2019_04_prescrire.pdf). 


Elle rapelle l'engagement de l'État à améliorer l'accessibilité de la base, et met en avant EurosForDocs

"

feature_row_5:
    - image_path: /assets/images/match_inter.png
      alt: "Paris Match & France Inter"
      excerpt: "

**30 avril 2019**


Vaccins anti-HPV : 15 médecins dénoncent des conflits d'intérêts 


En utilisant EurosForDocs, ils démontrent que les signataires d'un appel en faveur d'une généralisation des vaccins anti-HPV
ont reçu 1,6 millions d'euros de financement de la part des industriels produisant ces vaccins.


- [Site du contre-appel](https://spark.adobe.com/page/4cbZuGBONhmDC/)

- [Article de Paris Match](https://www.parismatch.com/Actu/Sante/Vaccins-anti-HPV-15-medecins-denoncent-les-risques-des-conflits-d-interets-1621133)

- [Chronique sur France Inter](https://www.franceinter.fr/emissions/sante-polemique/sante-polemique-07-mai-2019)

"

---


Le système de santé français est régulièrement secoué par des **crises sanitaires**, comme celle du Levothyrox depuis 2017. 

**Derrière les crises sanitaires se cachent souvent des conflits d'intérêts majeurs**. Les études scientifiques et les prescriptions médicales manquent d’indépendance, influencées par une industrie discrètement omniprésente.


Depuis 2010, le scandale du Médiator a amorcé une évolution. Les industriels doivent désormais déclarer tous leurs liens d'intérêts financiers dans la base **[Transparence-Santé](https://www.transparence.sante.gouv.fr/flow/interrogationAvancee?execution=e2s1)**.

<p style="text-align:center;">
<img 
    src="assets/images/base_transparence_sante.png" 
    alt="Base Transparence Santé" 
    width="600"
/>
</p>

Les déclarations sont extrêmement riches, mais le site public ne permet pas de les explorer facilement.

Au point que **ce dispositif de transparence est généralement perçu comme opaque**. 

Le collectif **Euros For Docs** simplifie l'accès à la base Transparence-Santé. 

<p style="text-align:center;">
    <a class="btn btn--success btn--large" href="/explore">Cliquer ici pour vous connecter à la plateforme </a>
</p>

Pour une **vision d'ensemble** sans connexion, cliquer sur l'image.
{: .text-center} 


<p style="text-align:center;">
<a href="https://www.eurosfordocs.fr/metabase/public/dashboard/0931cd9a-de38-4f17-b7d7-b05f56bf3f12">
<img 
    src="assets/images/global_view_metabase.png" 
    alt="Vision d'ensemble de la base Transparence-Santé"
    width="800"
/>
</a>
</p>

## Travaux exploitant EurosForDocs

{% include feature_row id="feature_row_1" type="left" %}

{% include feature_row id="feature_row_2" type="right" %}

{% include feature_row id="feature_row_3" type="left" %}

{% include feature_row id="feature_row_4" type="right" %}

{% include feature_row id="feature_row_5" type="left" %}


<p style="text-align:center;">
    <a class="btn btn--success" href="https://frama.link/EFD-Gslides">Ouvrir la présentation en plein écran</a>
</p>


<style>
.responsive-wrap iframe{ max-width: 95%; }
</style>
<div class="responsive-wrap">
<p style="text-align:center;">
<iframe 
    src="https://docs.google.com/presentation/d/e/2PACX-1vRKdaX6NCtVpldkAy1Jl2Ojyw1UaEYhy5Xqzxlaki364zi7fy0OmT7WLtUiYy2BxWyUm3opdHQMM0uy/embed?start=false&loop=false&delayms=1000" 
    frameborder="0"
    width="960" 
    height="569" 
    allowfullscreen="true" 
    mozallowfullscreen="true" 
    webkitallowfullscreen="true"
></iframe>
</p>
</div>