---
permalink: /explore/
title: Connexion à Metabase
toc: true
---

<p style="text-align:center;">
<img 
    src="{{ base }}/assets/images/alexander-andrews-636454-unsplash-resized.jpg" 
    alt="Map measure photo by Alexander Andrews on Unsplash" 
/>
</p>

EurosForDocs met à disposition l'interface Metabase[^metabase] pour explorer simplement la base Transparence-Santé.

[^metabase]: Metabase est une interface d'analyse et de visualisation de données. C'est un outil open-source, développé par une startup californienne, selon un modèle économique [freemium](https://fr.wikipedia.org/wiki/Freemium).  

L'accès à Metabase nécessite un compte[^authentification], que vous pouvez créer
- soit directement via le `sign-in Google`[^google], si vous avez une adresse email terminant par `@gmail.com` ;
- soit en écrivant à [`contact@eurosfordocs.fr`](mailto:contact@eurosfordocs.fr).

[^authentification]: L'obligation de se connecter est une limitation technique liées à l'utilisation de Metabase. L'objectif est d'offrir sans connexion, et donc sans Metabase, toutes les fonctionnalités de recherche. Les fonctionnalités d'analyse avancées demanderont elles toujours une connexion.

[^google]: L'outil Metabase est indépendant de Google. Seule la création de compte est simplifiée si vous disposez d'une adresse email terminant par `@gmail.com`.


▼ **CONNEXION** en bas. Merci de **LIRE** attentivement cette page **AVANT** ▼
{: .notice--success .text-center} 

## La base Transparence-Santé

### Restriction d'usage

L'utilisation de la base Transparence-Santé est soumise à des restrictions légales.

En utilisant Metabase, vous devenez _réutilisateur_ des données et devez respectez ces restrictions. 

En particulier, tout usage : 
- doit avoir pour finalité la transparence des liens d’intérêts,
- ne peut se faire à titre strictement commercial,
- doit citer la source des données.

Les détails légaux sont précisés dans la 
[licence]({{ base }}/download/Licence_reutilisation_donnees_transparence_sante.pdf).

### Erreurs dans les déclarations

**Attention !**
La base Transparence-Santé contient des déclarations 
[erronées](https://abonnes.lemonde.fr/les-decodeurs/article/2017/10/12/les-rates-de-la-base-de-donnees-publique-transparence-sante_5199937_4355770.html). 
{: .notice--danger}

L'exactitude des contenus publiés est de la responsabilité des entreprises ayant procédé aux déclarations. 

EurosForDocs ne peut être tenu responsable d'informations inexactes ou incomplètes issues de cette base.

En cas d'erreur, les benéficiaires peuvent faire valoir leur droit de rectification[^rectification] 
- sur le [site gouvernemental](https://www.transparence.sante.gouv.fr/) pour les personnes physiques,
- en s'adressant à l'entreprise pour les personnes morales.

Ces rectifications seront alors prises en compte sur EurosForDocs.  

[^rectification]: Sur le site gouvernemental, l'onglet "Mode d'emploi" explique dans la section "Modalités de recherche" comment procéder aux demandes de rectifications.

### Nettoyage par EurosForDocs 

Pour simplifier l'accès à la base Transparence-Santé, 
les données affichées dans EurosForDocs ont fait l'objet d'un nettoyage[^nettoyage]. 
- Ce nettoyage est en cours.
- Des erreurs de traitement sont possibles. 
- Malgré nos efforts de clarification, des erreurs d'interprétation sont vite arrivées. 

Pour toutes ces raisons, il est recommandé de **demander conseil** avant toute publication utilisant EurosForDocs.
{: .notice--danger}

[^nettoyage]: Le code informatique utilisé est disponible sur [GitLab](https://gitlab.com/eurosfordocs/transparence-sante). Nous demander si vous souhaitez des explications en français. 

## Vos données personnelles

Lorsque vous créez un compte Metabase
- Si vous utilisez le `sign-in Google`, nous accédons à votre adresse email[^email], votre nom et votre prénom[^nom]. 
Google est informé lors des connexions.
- Si vous nous demandez la création d'un compte, nous enregistrons votre adresse email[^email], 
ainsi que votre nom et votre prénom[^nom] si renseignés. 
Votre mot de passe est stocké de façon sécurisée[^passe].
   
Lorsque vous naviguez sur Metabase, l'outil enregistre votre activité[^activite].

_En vous connectant à Metabase, vous acceptez le traitement de ces informations personelles 
(précisions sur cette [page]({{ base }}/legal))._

[^nom]: Le nom et le prénom sont modifiables dans les réglages du compte utilisateur de Metabase.

[^activite]: L'outil Metabase trace la plupart de vos actions, comme d'ailleurs la plupart des sites internet. Nous utilisons ces informations pour suivre l'utilisation du site. 

[^passe]: Le mot de passe est [salé](https://fr.wikipedia.org/wiki/Salage_(cryptographie)) et hashé par Metabase. Pour **votre** sécurité, utilisez des mots de passe *différents* sur *tous* les sites internet.

[^email]: EurosForDocs n'a pas mis en place de newsletter email. Suivez nous sur les réseaux sociaux Twitter ou LinkedIn. EuroForDocs est susceptible de contacter les utilisateurs enregistrés sur Metabase.

## Utilisation de Metabase

### Première utilisation

Commencer par utiliser les tableaux de bord existants, notamment
- Vision par entreprise déclarante, 
- Vision par professionnel bénéficiaire,
- Vision par structure bénéficiaire. 

**Filtre** : 
Utiliser les filtres en haut à gauche pour restreindre les résultats affichés.  
{: .notice--success}

La **recherche** n'est pas sensible à la casse (minuscule vs MAJUSCULE).
En revanche, les valeurs sélectionnées sont sensibles à la casse. 
Les *copier-coller* ne fonctionneront donc pas. 
{: .notice--warning} 

### Se connecter

En vous connectant, vous déclarez avoir lu et compris les informations de cette page.

<p style="text-align:center;">
    <a class="btn btn--success btn--large" href="/metabase">Se connecter à Metabase</a>
</p>

### Utilisation avancée

Metabase permet de réaliser des analyses avancées sans compétences de programmation.

Nous vous invitons à parcourir la [documentation de Metabase](https://www.metabase.com/docs/latest/getting-started.html), 
à explorer par vous même, ou à nous contacter pour une rapide formation à distance.
