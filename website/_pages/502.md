---
permalink: /502.html
---

Metabase est actuellement en maintenance, merci de revenir plus tard.

Si le problème persiste, n'hésitez pas à écrire à [contact@eurosfordocs.fr](mailto:contact@eurosfordocs.fr).

Voici en attendant une vision d'ensemble de la base transparence-santé.

<p style="text-align:center;">
<img 
    src="assets/images/global_view_metabase.png" 
    alt="Vision d'ensemble de la base Transparence-Santé"
    width="800"
/>
</p>

[Retour à la page d'accueil](/){: .btn .btn--primary .btn--large}