#!/usr/bin/env bash
set -euo pipefail

docker system prune --force

docker-compose build python

# Reset data processing
docker-compose run --rm python ./main.py download --reset
docker-compose run --rm python ./main.py extract --reset
docker-compose run --rm python ./main.py clean --reset
docker-compose run --rm python ./main.py dump --reset

# Reset postgres
docker-compose up -d postgres
docker-compose stop metabase
docker-compose run --rm python ./main.py upload --reset

# Restart and refresh Metabase
docker-compose up -d metabase
docker-compose stop nginx
docker-compose up -d nginx
docker-compose run --rm python ./main.py load-cache --reset
